#! /bin/bash

httpx -fr --silent --no-color < $(echo "$1") | sed 's#\]\|\[##g' | tee | git "$2"
