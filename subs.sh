#/bin/bash

subfinder -d "$1" -o sf.json -json -cs;

gron -s sf.json | grep host | grepaddr | grep -v -Ei 'www\.' | sort -u | tee sf.txt;

assetfinder --subs-only "$1" | grep -v -Ei 'www\.' | sort -u | tee af.txt;

./alive.sh;

./gather.sh;
