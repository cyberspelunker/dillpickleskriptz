#! /bin/bash

echo "downloading table.txt and asns.csv from bgp.tools..."

wget https://bgp.tools/table.txt
wget https://bgp.tools/asns.csv

echo "...done"

echo "done"
echo "installing mapcidr"

go install -v github.com/projectdiscovery/mapcidr/cmd/mapcidr@latest

echo "setup finished"
