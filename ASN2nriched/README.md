```

    |      .|'''.|  '|.   '|'                        ||          '||                   '||  
   |||     ||..  '   |'|   |   /\  .. ...   ... ..  ...    ....   || ..     ....     .. ||  
  |  ||     ''|||.   | '|. |  (  )  ||  ||   ||' ''  ||  .|   ''  ||' ||  .|...||  .'  '||  
 .''''|.  .     '||  |   |||    //  ||  ||   ||      ||  ||       ||  ||  ||       |.   ||  
.|.  .||. |'....|'  .|.   '|   //  .||. ||. .||.    .||.  '|...' .||. ||.  '|...'  '|..'||. 
                              /(                                                            
                              {___                                                          

```

> Type an organization name or AS name text query get netblocks of nriched IP address json out

---

## setup

1. Install `nrich`

Grab the [latest release](https://gitlab.com/shodan-public/nrich/-/releases) for your operating system. For example, to install the ``nrich`` command in Ubuntu:

```shell
$ wget https://gitlab.com/api/v4/projects/33695681/packages/generic/nrich/latest/nrich_latest_amd64.deb
$ sudo dpkg -i nrich_latest_amd64.deb
```

For MacOS, install from tap repository with **homebrew**
```shell
$ brew tap shodan-public/homebrew-nrich https://gitlab.com/shodan-public/homebrew-nrich
$ brew install nrich
```

2. Get pre-requirments by following Automated or Manual instructions below.
 
**Automated:** run `sh setup.sh`

#### setup.sh requirments

I made a `setup.sh` file that *downloads* `table.txt` and `asns.csv` from bgp.tools using `wget` and 
*installs* `mapcidr` which I used to turn ASNs into CIDR netblocks into IPs 
`mapcidr` install requires `go`


*OR*

**Manual:** manually download [table.txt](https://bgp.tools/table.txt) & [asns.csv](https://bgp.tools/asns.csv) from bgp.tools and install [mapcidr](https://github.com/projectdiscovery/mapcidr)

3. `chmod +x asn2nriched.sh`

## Usage
Giveth string query and ye shall be blessed with enriched ip addresses

`./asn2nriched.sh Saskpower`

### Known issues
Any query that results in an ipz.txt file with more than 10,000 IP addresses or  running the tool to quickly with smaller ipz.txt counts that exceed 10,000 from quickly running the script or a combination of the two may result in empty output / json file output results not due to a blank entry on internetdb api data.

working on an auto batching patch to fix this, added getcidrz.sh to do every step without calling nrich for use to send into non nrich commands of your own choice which can be used for now to manually batch big output ranges.

## TODO
[x] add manual regex for cidr4 and ipv4 to make grepaddr optional
[ ] add cidr2iplist step with builtin unix binaries to make mapcidr optional
[x] HACKTHEPLANET
