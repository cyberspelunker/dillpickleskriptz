#! /bin/bash

mapcidr -silent -cl <(grep -Ei "$1" asns.csv | tee orgz.txt | grep -Ei -o '^AS[0-9]*' | sed 's#AS##g' | xargs -I {} grep -Ei ' {}$' table.txt | tee ASnetblocks.txt | grep -E -o "([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2}" | tee cidrz.txt) > ipz.txt
